/*
 * RedBlackTree.h
 *
 *  Created on: Feb 9, 2017
 *      Author: cheny_000
 */

#ifndef REDBLACKTREE_H
#define REDBLACKTREE_H
#include  <os.h>

//Define color enum
typedef enum c { RED,BLACK } COLOR;

/*
 * Node structure
 */

typedef struct _Node{
	OS_TCB* tcb;
	struct _Node *parentNode;
	struct _Node *rightNode;
	struct _Node *leftNode;
	COLOR color; //0 for black, 1 for red
}Node;

/*
 *  Heap Structure
 */
typedef struct _redBlackTree{
	Node *rootNode;
	int numberOfElements;
}RedBlackTree;



//Function Prototype
void initRedBlackTree();
void insertNode_RedBlackTree(OS_TCB*);

//Insert the node
void traverseInsert(Node *newNodw,Node *parentNode);

//Helper function in traverseInsert
//Traverse the tree in pre-order manner
void PreOrderTraverseRemaining(Node *rootNode);

//Print the tree in in-order manner
void printTree();

//Apply RedBlackTree properties
void Rearrange(Node *newNode);

//Delete Node
void deleteNode_RedBlackTree(int priority);

//Delete fix
void deleteFix(Node *p);

//Get minimum key
OS_TCB* getMin();

//Update remainingTime
void updateRemainingTime();


//Helper function
//Return current grandParent Node
Node* rotateRight(Node *);
Node* rotateLeft(Node *);
Node* successor(Node *p);

//Dynamic Array
static RedBlackTree redblacktree;
static Node node[5];

#endif /* REDBLACKTREE_H */
