/*
 * BinaryHeap.h
 *
 *  Created on: Feb 8, 2017
 *      Author: cheny_000
 */

#ifndef BINARYHEAP_H
#define BINARYHEAP_H
#include  <os.h>


/*
 *  Heap Structure
 */
typedef struct _binarySearchTree{
	OS_TCB* nodeList[5];
	int numberOfElements;
	int maxNumberOfTasks;
}BinarySearchTree;

//Function Prototype
void initBinarySearchTree(int);
void insertNode_BinaryTree(OS_TCB* _tcb);
void rearrange();
void heapify(int,OS_TCB **);
void swap(int,int,OS_TCB**);
void deleteNode_BinaryTree(OS_TCB* _tcb);
OS_TCB* peekRootNode();
OS_TCB* popRootNode();
void printBinarySearchTree();

//Dynamic Array
static BinarySearchTree binaryheap;



#endif /* BINARYHEAP_H_ */
