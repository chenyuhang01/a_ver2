/*
 * RedBlackTree.c
 *
 *  Created on: Feb 9, 2017
 *      Author: cheny_000
 */

#include "RedBlackTree.h"
#include "BinaryHeap.h"

void initRedBlackTree()
{
	//Create new binarySearchTree
	redblacktree.numberOfElements = 0;
	redblacktree.rootNode = 0;
}


void insertNode_RedBlackTree(OS_TCB *_tcb)
{
	//Get Root Node
	Node *rootNode = redblacktree.rootNode;
	Node *tempNode = 0;
	Node * newNode = 0;

	if(rootNode == 0)
	{
		rootNode = &node[redblacktree.numberOfElements];
		rootNode->tcb = _tcb;
		rootNode->leftNode = 0;
		rootNode->rightNode = 0;
		rootNode->parentNode = 0;
		rootNode->color = BLACK;
		redblacktree.rootNode = rootNode;
	}
	else
	{
		newNode = &node[redblacktree.numberOfElements];
		newNode->tcb = _tcb;
		newNode->leftNode = 0;
		newNode->rightNode = 0;
		newNode->parentNode = 0;

		newNode->color = RED;
		//Inserting to the binaryTree
		tempNode = rootNode;

		//Recursively Traverse and look for valid avaliable slot
		traverseInsert(newNode,tempNode);

		Rearrange(newNode);

	}
        redblacktree.rootNode;      
	redblacktree.numberOfElements++;

}
void traverseInsert(Node *newNode,Node *parentNode)
{
	if(newNode->tcb->remainingTime < parentNode->tcb->remainingTime)
	{
		if(parentNode->leftNode == 0)
		{
			parentNode->leftNode = newNode;
			newNode->parentNode = parentNode;
			return;
		}
		else
		{
			traverseInsert(newNode,parentNode->leftNode);
		}
	}
	else
	{
		if(parentNode->rightNode == 0)
		{
			parentNode->rightNode = newNode;
			newNode->parentNode = parentNode;
			return;
		}
		else
		{
			traverseInsert(newNode,parentNode->rightNode);
		}
	}
}


Node* rotateRight(Node *pivotNode)
{
	//Make Q as pivot Node
	Node *Q = pivotNode;

	//Make P as pivot Node's right child node
	Node *P = pivotNode->leftNode;

	//Q's right Node be P's left Node
	Q->leftNode = P->rightNode;

	if(Q->leftNode != 0)
		Q->leftNode->parentNode = Q;

	P->parentNode = Q->parentNode;

	if(Q->parentNode == 0)
		redblacktree.rootNode = P;
	else if(Q == Q->parentNode->leftNode)
		Q->parentNode->leftNode = P;
	else
		Q->parentNode->rightNode = P;

	P->rightNode= Q;
	Q->parentNode = P;

	return Q;
}



Node* rotateLeft(Node *pivotNode)
{
	//Make P as pivot Node
	Node *P = pivotNode;

	//Make Q as pivot Node's right child node
	Node *Q = pivotNode->rightNode;

	//P's right Node be Q's left Node
	P->rightNode = Q->leftNode;

	if(P->rightNode != 0)
		P->rightNode->parentNode = P;

	Q->parentNode = P->parentNode;

	if(P->parentNode == 0)
		redblacktree.rootNode = Q;
	else if(P == P->parentNode->leftNode)
		P->parentNode->leftNode = Q;
	else
		P->parentNode->rightNode = Q;

	Q->leftNode= P;
	P->parentNode = Q;

	return P;
}

void Rearrange(Node *newNode)
{
	//Get Root Node
	Node *rootNode = redblacktree.rootNode;

	//Declare temporary Nodes
	Node *parent = 0;
	Node *grandParent = 0;
	Node *uncle = 0;

	Node *curNode = newNode;

	//Contiune fix if
	//1. if the curNode is not root
	//2. if the curNode is red
	//3. if the parent of the curNode is red



	while((curNode != rootNode) && (curNode->color == RED) && (curNode->parentNode->color == RED))
	{
		//Get Parent node
		parent = newNode->parentNode;

		//Get grandParentNode
		grandParent = newNode->parentNode->parentNode;

	    /*  Case : A , Parent of newNode is left child of Grand-parent of newNode */
		if(parent == grandParent->leftNode)
		{

			//Get curNode's uncle
			uncle = grandParent->rightNode;

            /* Case : 1, The uncle of curNode is also red Only Recoloring required */
			if((uncle != 0) && (uncle->color == RED))
			{
				//Set curNode's grandparent to be red
				//Set curNode's parent to be black
				//Set curNode's uncle to be black

				grandParent->color = RED;
				parent->color = BLACK;
				uncle->color = BLACK;
				curNode = grandParent;
			}
			else
			{
				/* Case : 2, curNode is right child of its parent Left-rotation required */
				if(curNode == parent->rightNode)
				{
					rotateLeft(grandParent);
					curNode = parent;
					parent = curNode->parentNode;
				}
                /* Case : 3 curNode is left child of its parent Right-rotation required */
                rotateRight(grandParent);
                enum c colorTemp = parent->color;
                parent->color = grandParent->color;
                grandParent->color = colorTemp;
                curNode = parent;
			}
		}
		/* Case : B Parent of curNode is right child of Grand-parent of curNode */
		else
		{
			//Get curNode's uncle
			uncle = grandParent->leftNode;

            /* Case : 1, The uncle of curNode is also red Only Recoloring required */
			if((uncle != 0) && (uncle->color == RED))
			{
				//Set curNode's grandparent to be red
				//Set curNode's parent to be black
				//Set curNode's uncle to be black

				grandParent->color = RED;
				parent->color = BLACK;
				uncle->color = BLACK;
				curNode = grandParent;
			}
			else
			{
				/* Case : 2, curNode is left child of its parent Right-rotation required */
				if(curNode == parent->leftNode)
				{
					rotateRight(grandParent);
					curNode = parent;
					parent = curNode->parentNode;
				}
                /* Case : 3 curNode is right child of its parent Left-rotation required */
                rotateLeft(grandParent);
                enum c colorTemp = parent->color;
                parent->color = grandParent->color;
                grandParent->color = colorTemp;
                curNode = parent;
			}
		}
		rootNode = redblacktree.rootNode;
	}


	rootNode = redblacktree.rootNode;
	rootNode->color = BLACK;
}


void deleteNode_RedBlackTree(int remainingTime)
{
	//Get Root Node
	Node *rootNode = redblacktree.rootNode;
	Node *p,*y,*q = 0;
	int found=0;

	p = rootNode;
	y = 0;
	q = 0;

	//Find the Node to be deleted
    while(p!=0&&found==0)
    {
          if(p->tcb->remainingTime == remainingTime)
              found=1;
          if(found==0)
          {
                if(p->tcb->remainingTime < remainingTime)
                   p=p->rightNode;
                else
                   p=p->leftNode;
          }
    }

    if(found == 1)
    {
    	if(p->leftNode == 0 || p->rightNode == 0)
    	{
    		y = p;
    	}
    	else
    	{
    		y=successor(p);
    	}

    	if(y->leftNode != 0)
    	{
    		q=y->leftNode;
    	}
    	else
    	{
            if(y->rightNode!=0)
                 q=y->rightNode;
            else
                 q=0;
    	}
        if(q!=0)
               q->parentNode=y->parentNode;
          if(y->parentNode==0)
               rootNode=q;
          else
          {
              if(y==y->parentNode->leftNode)
                 y->parentNode->leftNode=q;
              else
                 y->parentNode->rightNode=q;
          }
          if(y!=p)
          {
              p->color=y->color;
              p->tcb =y->tcb;
          }
          if(y->color == BLACK)
        	  deleteFix(q);
    }


}

void deleteFix(Node *p)
{
	Node *temp = 0;
	Node *parent = 0;
	Node *grandparent = 0;

	//Get Root Node
	Node *rootNode = redblacktree.rootNode;

	while((p!=rootNode) && (p->color == BLACK))
	{
		parent = p->parentNode;
		grandparent = parent->parentNode;

		//if p is the left child of parent
		if(parent->leftNode == p)
		{
			//Let temp be the right child of p's parent if p is left child of parent
			temp = parent->rightNode;
			if(temp->color == RED)
			{
				temp->color = BLACK;
				parent->color = RED;
				rotateLeft(grandparent);
				temp = parent->rightNode;
			}
			if((temp->rightNode->color == BLACK) && (temp->leftNode->color == BLACK))
			{
				temp->color = RED;
				p = parent;
			}
			else
			{
				if(temp->rightNode->color == BLACK)
				{
					temp->leftNode->color = BLACK;
					temp->color = RED;
					rotateLeft(grandparent);
					temp = parent->rightNode;
				}
				temp->color = parent->color;
				parent->color = BLACK;
				temp->rightNode->color = BLACK;
				rotateLeft(grandparent);
				p = rootNode;
			}
		}
		else
		{
            temp = parent->leftNode;
            if(temp->color== RED)
            {
                  temp->color=BLACK;
                  parent->color= RED;
                  rotateRight(grandparent);
                  temp =parent->leftNode;
            }
            if(temp->leftNode->color== BLACK && temp->rightNode->color== BLACK)
            {
                  temp->color = RED;
                  p=parent;
            }
            else
            {
                  if(temp->leftNode->color==BLACK)
                  {
                        temp->rightNode->color= BLACK;
                        temp->color= RED;
                        rotateLeft(grandparent);
                        temp = parent->leftNode;
                  }
                  temp->color =parent->color;
                  parent->color=BLACK;
                  temp->leftNode->color= BLACK;
                  rotateRight(grandparent);
                  p = rootNode;
            }
		}
	    p->color= BLACK;
	    rootNode->color= BLACK;
	}

	redblacktree.rootNode = rootNode;
}

OS_TCB* getMin()
{
        

	//Get Root Node
	Node *rootNode = 0;

	Node *minNode = redblacktree.rootNode;
        
 	//Returned TCB
	OS_TCB *_tcb = 0;
        
        updateRemainingTime();
	
        while(minNode->leftNode != 0)
	{  
           if(minNode->tcb->remainingTime == 0)
           {    
              insertNode_BinaryTree(minNode->tcb);          
           }
           minNode = minNode->leftNode;
	}
        
	if(minNode!=0)
		_tcb = minNode->tcb;
        
	return _tcb;
}

Node* successor(Node *p)
{
     Node *y=0;
     if(p->leftNode!=0)
     {
         y=p->leftNode;
         while(y->rightNode!=0)
              y=y->rightNode;
     }
     else
     {
         y=p->rightNode;
         while(y->leftNode!=0)
              y=y->leftNode;
     }
     return y;
}

void PreOrderTraverseRemaining(Node *rootNode)
{
	if(rootNode == 0) return;
        
        if(rootNode->tcb->remainingTime == 0)
           rootNode->tcb->remainingTime = rootNode->tcb->period;
        
        rootNode->tcb->remainingTime--;
	
	PreOrderTraverseRemaining(rootNode->leftNode);
	PreOrderTraverseRemaining(rootNode->rightNode);
}

void updateRemainingTime()
{
  //Get Root Node
  Node *rootNode = redblacktree.rootNode;
  PreOrderTraverseRemaining(rootNode);  
}



